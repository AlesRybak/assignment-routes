package cz.alry.assignment.routes.data;

import java.util.List;

/**
 * Structure that holds countries and allows basic operations on it
 */
public interface CountriesStructure {

    /**
     * Checks that country with given code exists in the structure.
     * @param code Country code
     * @return true if exists, false otherwise
     */
    boolean existsByCode(String code);

    /**
     * Return list with one of the shortest routes for given start and target.
     *
     * The thing is, that there could be more routes, but we select the first and the the
     * best.
     *
     * @param start Starting country code
     * @param target Target country code
     * @return List of coutries or empty list if the route does not exist.
     */
    List<String> findShortestRoute(String start, String target);

}
