package cz.alry.assignment.routes.exception;

import java.net.URI;
import java.util.UUID;

/**
 * Exception thrown when invalid country is used.
 */
public class CountryNotFoundException extends RuntimeException {
    private final String countryCode;

    public CountryNotFoundException(String countryCode) {
        super("Country " + countryCode + " not found.");
        this.countryCode = countryCode;
    }

    public CountryNotFoundException(String countryCode, String customMessage) {
        super(customMessage);
        this.countryCode = countryCode;
    }

    public CountryNotFoundException(String countryCode, String customMessage, Throwable cause) {
        super(customMessage, cause);
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }
}
