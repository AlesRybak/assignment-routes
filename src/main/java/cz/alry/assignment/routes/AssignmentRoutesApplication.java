package cz.alry.assignment.routes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssignmentRoutesApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssignmentRoutesApplication.class, args);
	}

}
