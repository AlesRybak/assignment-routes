package cz.alry.assignment.routes.data;

import java.util.List;

/**
 * Mother object for creating country sets.
 */
public class CountryMother {

    public static final String CZECHIA = "CZE";
    public static final String SLOVAKIA = "SVK";
    public static final String HUNGARY = "HUN";
    public static final String AUSTRIA = "AUT";
    public static final String ITALY = "ITA";
    public static final String POLAND = "POL";
    public static final String MEXICO = "MEX";

    public static List<Country> emptyCountryList() {
        return List.of();
    }

    public static List<Country> czechOnlyCountryList() {
        return List.of(new Country(CZECHIA, List.of()));
    }

    public static List<Country> czechoslovakiaCountryList() {
        return List.of(
                new Country(CZECHIA, List.of(SLOVAKIA)),
                new Country(SLOVAKIA, List.of(CZECHIA)));
    }

    public static List<Country> vysegradCountryList() {
        return List.of(
                new Country(CZECHIA, List.of(POLAND, SLOVAKIA)),
                new Country(HUNGARY, List.of(SLOVAKIA)),
                new Country(POLAND, List.of(CZECHIA, SLOVAKIA)),
                new Country(SLOVAKIA, List.of(CZECHIA, HUNGARY, POLAND)));
    }

    public static List<Country> moreCountryList() {
        return List.of(
                new Country(AUSTRIA, List.of(CZECHIA, HUNGARY, ITALY, SLOVAKIA)),
                new Country(CZECHIA, List.of(AUSTRIA, POLAND, SLOVAKIA)),
                new Country(HUNGARY, List.of(AUSTRIA, SLOVAKIA)),
                new Country(ITALY, List.of(AUSTRIA)),
                new Country(POLAND, List.of(CZECHIA, SLOVAKIA)),
                new Country(SLOVAKIA, List.of(CZECHIA, HUNGARY, POLAND)),
                new Country(MEXICO, List.of()));
    }


}
