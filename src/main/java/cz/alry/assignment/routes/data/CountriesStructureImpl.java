package cz.alry.assignment.routes.data;

import java.util.List;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm.SingleSourcePaths;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

/**
 * Basic implementation of the CountriesStructure.
 *
 * Internally uses JGraphT library (https://jgrapht.org/) for doing the search of the shortest path.
 */
public class CountriesStructureImpl implements CountriesStructure {

    private Graph<String, DefaultEdge> countriesGraph;

    public CountriesStructureImpl(List<Country> countriesList) {
        prepareCountryGraph(countriesList);
    }

    private void prepareCountryGraph(List<Country> countriesList) {
        countriesGraph = new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);
        for (Country c : countriesList) {
            countriesGraph.addVertex(c.code());
        }
        for (Country c : countriesList) {
            for (String target : c.borders()) {
                countriesGraph.addEdge(c.code(), target);
            }
        }
    }

    @Override
    public boolean existsByCode(String code) {
        return countriesGraph.containsVertex(code);
    }

    @Override
    public List<String> findShortestRoute(String start, String target) {
        if (!existsByCode(start) || !existsByCode(target)) {
            return List.of();
        }

        if (start.equals(target)) {
            return List.of(start);
        }

        return findShortestRouteDijkstraAlg(start, target);
    }

    /**
     * Implementation using Dijkstra algorithm.
     *
     * There is no info, if the DijkstraShortestPath is thread safe, so we create new instance
     * each time.
     */
    private List<String> findShortestRouteDijkstraAlg(String start, String target) {
        DijkstraShortestPath<String, DefaultEdge> dijkstraAlg = new DijkstraShortestPath<>(countriesGraph);
        SingleSourcePaths<String, DefaultEdge> iPaths = dijkstraAlg.getPaths(start);
        final GraphPath<String, DefaultEdge> shortestPath = iPaths.getPath(target);

        return (shortestPath == null)
                ? List.of()
                : shortestPath.getVertexList();
    }
}
