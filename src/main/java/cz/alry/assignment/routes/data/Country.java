package cz.alry.assignment.routes.data;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Holds information about countries.
 */
public record Country(
        @JsonProperty("cca3") String code,
        List<String> borders) {
}
