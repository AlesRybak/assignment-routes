package cz.alry.assignment.routes.resource;

import java.util.List;
import java.util.Optional;
import cz.alry.assignment.routes.service.RoutesService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/routing")
public class RoutingResource {

    private final RoutesService routesService;

    @GetMapping("/{start}/{target}")
    public Optional<List<String>> routing(
            @PathVariable String start,
            @PathVariable String target) {
        return routesService.searchRoute(start, target);
    }

}